# JIRA Service Desk User Menu Action add-on example.

Welcome to this add-on example.

This add-on example shows you how you can add an action to the JIRA Service Desk user menu.

We are using the ACE ([Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express)) framework. 

## Want to know more about adding an action in the JIRA Service Desk user menu?

    [Adding your action to the JIRA Service Desk user menu](https://developer.atlassian.com/jiracloud/jira-service-desk-modules-customer-portal-39988271.html#JIRAServiceDeskmodules-Customerportal-Usermenuaction).